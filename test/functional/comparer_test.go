package functional

import (
	"context"
	"encoding/json"
	"github.com/nsf/jsondiff"
	model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/transport"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service/comparer"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/storage/datacsv"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/storage/filelocal"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestFunctional_Compare(t *testing.T) {
	dataStore := datacsv.New()
	fileStore := filelocal.New()
	svc := comparer.New(fileStore, dataStore)

	ctx := context.Background()
	w := httptest.NewRecorder()

	compareEndp := func(ctx context.Context, req interface{}) (interface{}, error) {
		resp, err := svc.Compare(ctx, req.(model.CompareRequest))
		if err != nil {
			return nil, err
		}

		return transport.CompareResponse{}.ToTransportModel(resp), nil
	}

	decodeRequest := func(r *http.Request) (interface{}, error) {
		pr := transport.CompareRequest{}
		if err := json.NewDecoder(r.Body).Decode(&pr); err != nil {
			return nil, err
		}

		return pr.ToServiceModel(), nil
	}

	encodeResponse := func(w http.ResponseWriter, response interface{}) error {
		w.Header().Set("content-Type", "application/json; charset=utf-8")
		return json.NewEncoder(w).Encode(response)
	}

	handler := func(w http.ResponseWriter, r *http.Request) {
		req, err := decodeRequest(r)
		if err != nil {
			t.Fatal("error decode request", err)
		}

		resp, err := compareEndp(ctx, req)
		if err != nil {
			t.Fatal("error work endpoint", err)
		}

		err = encodeResponse(w, resp)
		if err != nil {
			t.Fatal("error work endpoint", err)
		}
	}

	bodyReader := strings.NewReader(`
	{
		"old_file_path": "../../test/fixtures/big_data_old.csv",
		"new_file_path": "../../test/fixtures/big_data_new.csv"
	}
`)
	req := httptest.NewRequest("POST", "http://host/api/v1/compare", bodyReader)
	handler(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	expBody := `
{
    "created": [
        {
            "name": "Mazeratti",
            "description": "Elite car",
            "price": "2000"
        }
    ],
    "deleted": [
        {
            "name": "Ferrari",
            "description": "Elite car",
            "price": "100000"
        },
        {
            "name": "Honda",
            "description": "Family car",
            "price": "100"
        }
    ],
    "updated": [
        {
            "name": "Audi",
            "updated_fields": [
                {
                    "field_name": "price",
                    "old_value": "1000",
                    "new_value": "1100"
                }
            ]
        }
    ]
}`
	opts := jsondiff.DefaultJSONOptions()
	diff, _ := jsondiff.Compare(body, []byte(expBody), &opts)
	if diff != jsondiff.FullMatch {
		t.Fatal("body != expBody")
	}
}
