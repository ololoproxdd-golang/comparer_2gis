package config

import (
	"github.com/jessevdk/go-flags"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
	"os"
)

type Config struct {
	AppEnv      string `short:"e" long:"app-env" env:"APP_ENV" description:"Application env name"`
	AppHost     string `short:"h" long:"app-host" env:"APP_HOST" description:"Application server host"`
	AppHttpPort string `short:"g" long:"app-http-port" env:"APP_HTTP_PORT" description:"Gateway http/rest port"`
	LogPath     string `short:"l" long:"log-path" env:"LOG_PATH" description:"Filename for logs"`

	HostName string

	Test string `short:"t" long:"is_test"`
}

func LoadConfig() Config {
	var config Config
	var err error

	err = godotenv.Load()
	if err != nil {
		log.Fatal().Err(err).Msg("cant load env")
	}

	parser := flags.NewParser(&config, flags.Default)
	if _, err = parser.Parse(); err != nil {
		log.Fatal().Err(err).Msg("cant parse env")
	}

	config.HostName = getHostName()

	return config
}

func getHostName() string {
	hostName, err := os.Hostname()
	if err != nil {
		log.Warn().Err(err).Msg("cant get hostname")
	}

	return hostName
}
