package server

import (
	"github.com/gorilla/mux"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/middleware"
)

func newHttpMiddleware() []mux.MiddlewareFunc {
	return []mux.MiddlewareFunc{
		middleware.LoggerHTTP,
		middleware.RecoveryHTTP,
	}
}
