package server

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"io"
	"os"
)

func (s *server) initLogger() {
	var writer io.Writer

	zerolog.MessageFieldName = "MESSAGE"
	zerolog.LevelFieldName = "LEVEL"
	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	writer = os.Stdout
	fw, err := os.OpenFile(s.config.LogPath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, os.FileMode(0666))
	if err != nil {
		log.Error().Err(err).Msg("cant open file to logs")
	} else {
		writer = io.MultiWriter(writer, io.Writer(fw))
	}

	log.Logger = log.Output(writer).
		With().Str("PROGRAM", s.config.AppEnv).
		Str("HOST_NAME", s.config.HostName).
		Logger()
}
