package server

import (
	"context"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/config"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/metrics"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service/comparer"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service/health"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/transport"
	"net/http"
	"sync"
	"sync/atomic"
	"time"
)

const (
	stateNew int32 = iota + 1
	stateRun
	stateClosed
)

type server struct {
	config config.Config

	endpoints *transport.Endpoints

	httpServer *http.Server

	service struct {
		comparer comparer.Service
		health   health.Service
	}

	storage struct {
		file service.FileStorage
		data service.DataStorage
	}

	state int32

	monitorCollector *metrics.Monitor
}

func NewServer(config config.Config) *server {
	s := &server{config: config, state: stateNew}
	s.initLogger()
	return s
}

func (s *server) Run() {
	if !atomic.CompareAndSwapInt32(&s.state, stateNew, stateRun) {
		log.Error().Msg("cant run server: server is not opened")
		return
	}

	log.Info().Msg("running service comparer_2gis")
	s.registerOsSignal()

	wg := sync.WaitGroup{}
	wg.Add(1)
	go s.runHttpServer(&wg)

	wg.Wait()
	log.Info().Msg("comparer_2gis stopped ...")
}

func (s *server) runHttpServer(wait *sync.WaitGroup) {
	defer wait.Done()
	log.Info().Msgf("running http server address: %s ...", s.getTransportHttp().Addr)

	err := s.getTransportHttp().ListenAndServe()
	log.Info().Err(err).Msg("closed http server ... ")
}

func (s *server) Close() {
	var err error
	if !atomic.CompareAndSwapInt32(&s.state, stateRun, stateClosed) {
		log.Error().Msg("server stopped or not running")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err = s.httpServer.Shutdown(ctx); err != nil {
		log.Warn().Err(err).Msg("closing http server error")
	}

	log.Info().Msg("server stopped ...")
}
