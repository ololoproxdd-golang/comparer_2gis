package server

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/metrics"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service/comparer"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service/health"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/storage/datacsv"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/storage/filelocal"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/transport"
	"net/http"
)

func (s *server) getMonitor() *metrics.Monitor {
	if s.monitorCollector != nil {
		return s.monitorCollector
	}
	s.monitorCollector = metrics.NewMonitor("test", s.config.AppEnv, s.config.HostName)
	log.Info().Msg("created monitor ...")
	return s.monitorCollector
}

func (s *server) getTransportHttp() *http.Server {
	if s.httpServer != nil {
		return s.httpServer
	}

	mux := http.NewServeMux()
	middleware := newHttpMiddleware()
	mux.Handle("/api/v1/", transport.MakeHttpHandler(s.getEndpoints(), middleware...))
	mux.Handle("/metrics", s.getMonitor().Handler())

	addr := s.config.AppHost + ":" + s.config.AppHttpPort
	s.httpServer = &http.Server{Addr: addr, Handler: mux}

	log.Info().Msgf("created server transport - http %s ... ", s.httpServer.Addr)
	return s.httpServer
}

func (s *server) getEndpoints() *transport.Endpoints {
	if s.endpoints != nil {
		return s.endpoints
	}

	s.endpoints = transport.NewEndpoints(s.getCompareService(), s.getHealthService())

	log.Info().Msg("created endpoints ...")
	return s.endpoints
}

func (s *server) getHealthService() health.Service {
	if s.service.health != nil {
		return s.service.health
	}

	s.service.health = health.New()

	log.Info().Msg("created health service ...")
	return s.service.health
}

func (s *server) getCompareService() comparer.Service {
	if s.service.health != nil {
		return s.service.comparer
	}

	s.service.comparer = comparer.New(s.getFileStorage(), s.getDataStorage())
	s.service.comparer = comparer.NewMetrics(s.getMonitor())(s.service.comparer)

	log.Info().Msg("created comparer service with metrics ...")
	return s.service.comparer
}

func (s *server) getFileStorage() service.FileStorage {
	if s.storage.file != nil {
		return s.storage.file
	}

	s.storage.file = filelocal.New()
	s.storage.file = filelocal.NewMetrics(s.getMonitor())(s.storage.file)

	log.Info().Msg("created file storage with metrics ...")
	return s.storage.file
}

func (s *server) getDataStorage() service.DataStorage {
	if s.storage.data != nil {
		return s.storage.data
	}

	s.storage.data = datacsv.New()
	s.storage.data = datacsv.NewMetrics(s.getMonitor())(s.storage.data)

	log.Info().Msg("created data storage with metrics ...")
	return s.storage.data
}
