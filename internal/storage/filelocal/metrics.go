package filelocal

import (
	"context"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/metrics"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service"
	"os"
	"time"
)

const methodRead = "storage/filelocal/Read"

type metricMiddleware struct {
	service.FileStorage
	monitor *metrics.Monitor
}

func NewMetrics(monitor *metrics.Monitor) service.FileStorageMiddleware {
	return func(next service.FileStorage) service.FileStorage {
		return &metricMiddleware{
			FileStorage: next,
			monitor:     monitor,
		}
	}
}

func (m *metricMiddleware) Read(ctx context.Context, filename string) (*os.File, error) {
	defer m.monitor.Metrics.DurationInc(methodRead, time.Now())
	resp, err := m.FileStorage.Read(ctx, filename)
	m.monitor.Metrics.CountInc(methodRead, err)
	return resp, err
}
