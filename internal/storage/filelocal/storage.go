package filelocal

import (
	"context"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service"
	"os"
)

var errReadFile = errors.New("read file from local storage error")

type storage struct{}

func New() service.FileStorage {
	return &storage{}
}

func (s storage) Read(_ context.Context, filename string) (*os.File, error) {
	file, err := os.Open(filename)
	if err != nil {
		log.Error().Err(err).Msg(errReadFile.Error())
		return nil, errReadFile
	}

	return file, nil
}
