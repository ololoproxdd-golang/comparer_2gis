package filelocal

import (
	"context"
	"os"
	"reflect"
	"testing"
)

func TestUnit_Storage_Read(t *testing.T) {
	storage := New()

	var flagtests = []struct {
		testName     string
		ctx          context.Context
		fileName     string
		expFileExist bool
		expErr       error
	}{
		{
			testName:     "file not exist",
			ctx:          context.Background(),
			fileName:     "not_exist_file.csv",
			expFileExist: false,
			expErr:       errReadFile,
		},
		{
			testName:     "file exist",
			ctx:          context.Background(),
			fileName:     "../../../test/fixtures/some_data.csv",
			expFileExist: true,
			expErr:       nil,
		},
	}

	var file *os.File
	var err error
	for _, tt := range flagtests {
		t.Run(tt.testName, func(t *testing.T) {
			file, err = storage.Read(tt.ctx, tt.fileName)
			var fileExist bool
			if file != nil {
				fileExist = true
			}
			if !reflect.DeepEqual(tt.expFileExist, fileExist) {
				t.Fatal("tt.expFileExist != fileExist")
			}
			if !reflect.DeepEqual(err, tt.expErr) {
				t.Fatal("tt.expErr != err")
			}
		})
	}
}
