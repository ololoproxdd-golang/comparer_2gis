package datacsv

import (
	"context"
	"encoding/csv"
	"github.com/pkg/errors"
	model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service"
	"io"
	"os"
)

const csvSeparator = ';'

var (
	errFileEmpty  = errors.New("input file is empty, read data error")
	errDataFormat = errors.New("read first row header error")
)

type storage struct{}

func New() service.DataStorage {
	return &storage{}
}

func (s storage) Read(_ context.Context, file *os.File) (map[string]*model.Data, error) {
	if file == nil {
		return nil, errFileEmpty
	}

	reader := csv.NewReader(file)
	reader.Comma = csvSeparator

	//read first row header
	_, err := reader.Read()
	if err != nil {
		return nil, errDataFormat
	}

	resp := make(map[string]*model.Data)
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, errors.Wrap(err, "read data from csv file error")
		}

		resp[record[0]] = &model.Data{
			Name:        record[0],
			Description: record[1],
			Price:       record[2],
		}
	}

	return resp, nil
}
