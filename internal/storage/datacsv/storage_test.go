package datacsv

import (
	"context"
	model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"
	"os"
	"reflect"
	"testing"
)

func TestUnit_Storage_Read(t *testing.T) {
	storage := New()

	var flagtests = []struct {
		testName string
		ctx      context.Context
		filePath string
		expData  map[string]*model.Data
		expErr   error
	}{
		{
			testName: "file not exist",
			ctx:      context.Background(),
			filePath: "file_not_exist.csv",
			expData:  nil,
			expErr:   errFileEmpty,
		},
		{
			testName: "file exist with few data",
			ctx:      context.Background(),
			filePath: "../../../test/fixtures/some_data.csv",
			expData: map[string]*model.Data{
				"Телефон Apple Iphone 15": {
					Name:        "Телефон Apple Iphone 15",
					Description: "Новинка",
					Price:       "144999",
				},
				"Телефон Apple Iphone 10xs": {
					Name:        "Телефон Apple Iphone 10xs",
					Description: "Старинка",
					Price:       "44999",
				},
			},
			expErr: nil,
		},
		{
			testName: "file exist without header",
			ctx:      context.Background(),
			filePath: "../../../test/fixtures/empty_data.csv",
			expData:  nil,
			expErr:   errDataFormat,
		},
	}

	var file *os.File
	var data map[string]*model.Data
	var err error
	for _, tt := range flagtests {
		t.Run(tt.testName, func(t *testing.T) {
			file, _ = os.Open(tt.filePath)
			data, err = storage.Read(tt.ctx, file)
			if !reflect.DeepEqual(tt.expData, data) {
				t.Fatal("tt.expData != data")
			}
			if !reflect.DeepEqual(err, tt.expErr) {
				t.Fatal("tt.expErr != err")
			}
		})
	}
}
