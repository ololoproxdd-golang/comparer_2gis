package datacsv

import (
	"context"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/metrics"
	model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service"
	"os"
	"time"
)

const methodRead = "storage/datacsv/Read"

type metricMiddleware struct {
	service.DataStorage
	monitor *metrics.Monitor
}

func NewMetrics(monitor *metrics.Monitor) service.DataStorageMiddleware {
	return func(next service.DataStorage) service.DataStorage {
		return &metricMiddleware{
			DataStorage: next,
			monitor:     monitor,
		}
	}
}

func (m *metricMiddleware) Read(ctx context.Context, file *os.File) (map[string]*model.Data, error) {
	defer m.monitor.Metrics.DurationInc(methodRead, time.Now())
	resp, err := m.DataStorage.Read(ctx, file)
	m.monitor.Metrics.CountInc(methodRead, err)
	return resp, err
}
