package transport

import (
	"context"
	"encoding/json"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/transport"
	"net/http"
)

func MakeHttpHandler(endpoints *Endpoints, middleware ...mux.MiddlewareFunc) http.Handler {
	r := mux.NewRouter()
	r.Use(middleware...)

	opts := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(NewErrorHandler("http")),
		kithttp.ServerErrorEncoder(encodeError),
	}

	pingHandler := kithttp.NewServer(
		endpoints.Ping,
		noDecodeRequest,
		encodeResponse,
		opts...,
	)

	compareHandler := kithttp.NewServer(
		endpoints.Compare,
		decodeFromDataRequest,
		encodeResponse,
		opts...,
	)

	r.Handle("/api/v1/ping", pingHandler).Methods("GET")
	r.Handle("/api/v1/compare", compareHandler).Methods("POST")
	return r
}

func noDecodeRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func decodeFromDataRequest(_ context.Context, r *http.Request) (interface{}, error) {
	pr := transport.CompareRequest{}
	if err := json.NewDecoder(r.Body).Decode(&pr); err != nil {
		return nil, err
	}

	return pr.ToServiceModel(), nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}

	w.Header().Set("content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}
