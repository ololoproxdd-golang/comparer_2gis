package transport

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/transport"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service/comparer"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service/health"
)

type Endpoints struct {
	Ping    endpoint.Endpoint
	Compare endpoint.Endpoint
}

func NewEndpoints(c comparer.Service, h health.Service) *Endpoints {
	return &Endpoints{
		Ping:    makePingEndpoint(h),
		Compare: makeCompareEndpoint(c),
	}
}

func makePingEndpoint(h health.Service) endpoint.Endpoint {
	return func(ctx context.Context, _ interface{}) (interface{}, error) {
		return h.Ping(ctx), nil
	}
}

func makeCompareEndpoint(c comparer.Service) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		r, ok := req.(model.CompareRequest)
		if !ok {
			err := errors.New("makeCompareEndpoint not type of model.CompareRequest")
			log.Error().Interface("REQUEST", req).Err(err)
			return nil, err
		}

		resp, err := c.Compare(ctx, r)
		if err != nil {
			return nil, err
		}

		return transport.CompareResponse{}.ToTransportModel(resp), nil
	}
}
