package transport

import model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"

type Data struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Price       string `json:"price"`
}

func (Data) ToTransportModel(d *model.Data) *Data {
	return &Data{
		Name:        d.Name,
		Description: d.Description,
		Price:       d.Price,
	}
}

type UpdatedData struct {
	Name      string          `json:"name"`
	UpdFields []*UpdatedField `json:"updated_fields"`
}

type UpdatedField struct {
	FiledName string `json:"field_name"`
	OldValue  string `json:"old_value"`
	NewValue  string `json:"new_value"`
}

func (UpdatedData) ToTransportModel(d *model.UpdatedData) *UpdatedData {
	resp := &UpdatedData{
		Name: d.New.Name,
	}

	if d.Old.Price != d.New.Price {
		resp.UpdFields = append(resp.UpdFields, &UpdatedField{
			FiledName: "price",
			OldValue:  d.Old.Price,
			NewValue:  d.New.Price,
		})
	}

	if d.Old.Description != d.New.Description {
		resp.UpdFields = append(resp.UpdFields, &UpdatedField{
			FiledName: "description",
			OldValue:  d.Old.Description,
			NewValue:  d.New.Description,
		})
	}

	return resp
}
