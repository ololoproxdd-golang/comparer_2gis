package transport

import model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"

type CompareRequest struct {
	Old string `json:"old_file_path"`
	New string `json:"new_file_path"`
}

func (pr CompareRequest) ToServiceModel() model.CompareRequest {
	return model.CompareRequest{
		Old: pr.Old,
		New: pr.New,
	}
}

type CompareResponse struct {
	Created []*Data        `json:"created"`
	Deleted []*Data        `json:"deleted"`
	Updated []*UpdatedData `json:"updated"`
}

func (CompareResponse) ToTransportModel(r *model.CompareResponse) *CompareResponse {
	created := make([]*Data, 0, len(r.Created))
	for _, v := range r.Created {
		created = append(created, Data{}.ToTransportModel(v))
	}

	deleted := make([]*Data, 0, len(r.Deleted))
	for _, v := range r.Deleted {
		deleted = append(deleted, Data{}.ToTransportModel(v))
	}

	updated := make([]*UpdatedData, 0, len(r.Updated))
	for _, v := range r.Updated {
		updated = append(updated, UpdatedData{}.ToTransportModel(v))
	}

	return &CompareResponse{
		Created: created,
		Deleted: deleted,
		Updated: updated,
	}
}
