package service

type Data struct {
	Name        string
	Description string
	Price       string
}

type UpdatedData struct {
	Old *Data
	New *Data
}
