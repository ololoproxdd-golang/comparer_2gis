package service

type CompareRequest struct {
	Old string
	New string
}

type CompareResponse struct {
	Created map[string]*Data
	Updated map[string]*UpdatedData
	Deleted map[string]*Data
}
