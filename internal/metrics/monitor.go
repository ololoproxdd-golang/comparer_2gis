package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog/log"
	"net/http"
)

type Monitor struct {
	Metrics  *Metrics
	Register *prometheus.Registry

	constLabels prometheus.Labels
	collector   prometheus.Collector
}

func NewMonitor(system, service, hostname string) *Monitor {
	m := &Monitor{constLabels: prometheus.Labels{"system": system, "service": service, "host": hostname}}
	m.Register = prometheus.NewRegistry()
	m.collector = prometheus.NewGoCollector()

	err := m.Register.Register(m.collector)
	if err != nil {
		log.Err(err).Msg("register new go collector error")
		return nil
	}

	m.Metrics = newMetrics(m.Register, m.constLabels)
	return m
}

func (m *Monitor) Handler() http.Handler {
	return promhttp.HandlerFor(m.Register, promhttp.HandlerOpts{})
}
