package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog/log"
	"time"
)

type Metrics struct {
	CountSuccess      *prometheus.CounterVec
	CountError        *prometheus.CounterVec
	DurationHistogram *prometheus.HistogramVec

	Counters    map[string]*prometheus.CounterVec
	constLabels prometheus.Labels
	registerer  prometheus.Registerer
}

func newMetrics(registerer prometheus.Registerer, constLabels prometheus.Labels) *Metrics {
	labels := []string{"method"}
	m := &Metrics{
		CountSuccess: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:        "request_success",
			Help:        "Counter success request",
			ConstLabels: constLabels,
		}, labels),
		CountError: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:        "request_error",
			Help:        "Counter error request",
			ConstLabels: constLabels,
		}, labels),
		DurationHistogram: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:        "request_duration_histogram",
			Help:        "Request duration histogram by seconds",
			ConstLabels: constLabels,
			Buckets: []float64{
				0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009,
				0.01, 0.015, 0.02, 0.025, 0.03, 0.035, 0.04, 0.045, 0.05, 0.055,
				0.06, 0.065, 0.07, 0.075, 0.08, 0.085, 0.09, 0.095,
				0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55,
				0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95,
				1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.8, 1.9,
				2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0},
		}, labels),
		constLabels: constLabels,
		registerer:  registerer,
	}

	if err := registerer.Register(m.CountSuccess); err != nil {
		log.Error().Err(err).Msg("register CountSuccess")
	}
	if err := registerer.Register(m.CountError); err != nil {
		log.Error().Err(err).Msg("register CountError")
	}

	if err := registerer.Register(m.DurationHistogram); err != nil {
		log.Error().Err(err).Msg("register DurationHistogram")
	}

	m.Counters = make(map[string]*prometheus.CounterVec)
	return m
}

func (m *Metrics) DurationInc(method string, begin time.Time) {
	m.DurationHistogram.WithLabelValues(method).Observe(time.Since(begin).Seconds())
}

func (m *Metrics) CountInc(method string, err error) {
	if err != nil {
		m.CountError.WithLabelValues(method).Inc()
	} else {
		m.CountSuccess.WithLabelValues(method).Inc()
	}
}

func (m *Metrics) RegisterCounter(name, help string, labels []string) error {
	counter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Name:        name,
		Help:        help,
		ConstLabels: m.constLabels,
	}, labels)
	if err := m.registerer.Register(counter); err != nil {
		return err
	}
	m.Counters[name] = counter
	return nil
}

func (m *Metrics) AddCounterValue(name string, value float64, labelValues ...string) {
	counter, ok := m.Counters[name]
	if !ok {
		return
	}
	if len(labelValues) == 0 {
		return
	}
	counter.WithLabelValues(labelValues...).Add(value)
}
