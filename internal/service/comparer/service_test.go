package comparer

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/test/mock"
	"os"
	"reflect"
	"testing"
)

func TestUnit_Service_Compare(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	fileStore := mock.NewMockFileStorage(ctrl)
	dataStore := mock.NewMockDataStorage(ctrl)
	service := New(fileStore, dataStore)

	var ctx context.Context
	var request model.CompareRequest
	var response, expResp *model.CompareResponse
	var err, mockErr, expErr error

	t.Run("read file error", func(t *testing.T) {
		ctx = context.Background()
		request = model.CompareRequest{
			Old: "old.csv",
			New: "new.csv",
		}

		mockErr = errors.New("read file error")
		fileStore.EXPECT().Read(ctx, gomock.AssignableToTypeOf(request.New)).Times(2).Return(nil, mockErr)

		expErr = errReadData
		expResp = nil
		response, err = service.Compare(ctx, request)
		if !reflect.DeepEqual(response, expResp) {
			t.Fatal("response != expResp")
		}
		if !errors.Is(err, expErr) {
			t.Fatal("err != expErr")
		}
	})

	t.Run("file is empty", func(t *testing.T) {
		ctx = context.Background()
		request = model.CompareRequest{
			Old: "old.csv",
			New: "new.csv",
		}

		mockErr = errors.New("file is empty")
		fileStore.EXPECT().Read(ctx, gomock.AssignableToTypeOf(request.New)).Times(2).Return(nil, nil)
		dataStore.EXPECT().Read(ctx, nil).Times(2).Return(nil, mockErr)

		expErr = errReadData
		expResp = nil
		response, err = service.Compare(ctx, request)
		if !reflect.DeepEqual(response, expResp) {
			t.Fatal("response != expResp")
		}
		if !errors.Is(err, expErr) {
			t.Fatal("err != expErr")
		}
	})

	t.Run("data is empty", func(t *testing.T) {
		ctx = context.Background()
		request = model.CompareRequest{
			Old: "old.csv",
			New: "new.csv",
		}

		fileStore.EXPECT().Read(ctx, gomock.AssignableToTypeOf(request.New)).Times(2).Return(nil, nil)
		dataStore.EXPECT().Read(ctx, nil).Times(2).Return(nil, nil)

		expErr = errDataEmpty
		expResp = nil
		response, err = service.Compare(ctx, request)
		if !reflect.DeepEqual(response, expResp) {
			t.Fatal("response != expResp")
		}
		if !errors.Is(err, expErr) {
			t.Fatal("err != expErr")
		}
	})

	t.Run("successful compare data", func(t *testing.T) {
		ctx = context.Background()
		request = model.CompareRequest{
			Old: "old.csv",
			New: "new.csv",
		}

		fileStore.EXPECT().Read(ctx, gomock.AssignableToTypeOf(request.New)).Times(2).Return(&os.File{}, nil)
		dataStore.EXPECT().Read(ctx, &os.File{}).Return(map[string]*model.Data{
			"Телефон Apple Iphone 15": {
				Name:        "Телефон Apple Iphone 15",
				Description: "Новинка",
				Price:       "144999",
			},
			"Телефон Apple Iphone 10xs": {
				Name:        "Телефон Apple Iphone 10xs",
				Description: "Старинка",
				Price:       "44999",
			},
		}, nil)

		dataStore.EXPECT().Read(ctx, &os.File{}).Return(map[string]*model.Data{
			"Телефон Apple Iphone 10xs": {
				Name:        "Телефон Apple Iphone 10xs",
				Description: "Старинка(2нед)",
				Price:       "40999",
			},
			"Apple Macbook": {
				Name:        "Apple Macbook",
				Description: "Хит",
				Price:       "140999",
			},
		}, nil)

		expErr = nil
		expResp = &model.CompareResponse{
			Created: map[string]*model.Data{
				"Apple Macbook": {
					Name:        "Apple Macbook",
					Description: "Хит",
					Price:       "140999",
				},
			},
			Updated: map[string]*model.UpdatedData{
				"Телефон Apple Iphone 10xs": {
					Old: &model.Data{
						Name:        "Телефон Apple Iphone 10xs",
						Description: "Старинка",
						Price:       "44999",
					},
					New: &model.Data{
						Name:        "Телефон Apple Iphone 10xs",
						Description: "Старинка(2нед)",
						Price:       "40999",
					},
				},
			},
			Deleted: map[string]*model.Data{
				"Телефон Apple Iphone 15": {
					Name:        "Телефон Apple Iphone 15",
					Description: "Новинка",
					Price:       "144999",
				},
			},
		}
		response, err = service.Compare(ctx, request)
		if !reflect.DeepEqual(response, expResp) {
			t.Fatal("response != expResp")
		}
		if !errors.Is(err, expErr) {
			t.Fatal("err != expErr")
		}
	})
}
