package comparer

import (
	"context"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/service"
	"sync"
)

var (
	errReadData  = errors.New("read data error")
	errDataEmpty = errors.New("one/both files is empty")
)

type comparer struct {
	fileStore service.FileStorage
	dataStore service.DataStorage
}

func New(fs service.FileStorage, ds service.DataStorage) Service {
	return &comparer{
		fileStore: fs,
		dataStore: ds,
	}
}

func (s comparer) Compare(ctx context.Context, r model.CompareRequest) (*model.CompareResponse, error) {
	var wg sync.WaitGroup
	var newData, oldData map[string]*model.Data
	var err error

	cNewData := make(chan map[string]*model.Data)
	cOldData := make(chan map[string]*model.Data)
	cErr := make(chan error)
	cDone := make(chan struct{})
	go func() {
		go s.read(ctx, r.New, &wg, cNewData, cErr)
		wg.Add(1)

		go s.read(ctx, r.Old, &wg, cOldData, cErr)
		wg.Add(1)

		wg.Wait()
		cDone <- struct{}{}
	}()

	for {
		select {
		case newData = <-cNewData:
		case oldData = <-cOldData:
		case e := <-cErr:
			err = multierror.Append(err, e)
		case <-cDone:
			close(cNewData)
			close(cOldData)
			close(cErr)
			close(cDone)
			goto AfterLoop
		}
	}
AfterLoop:

	if err != nil {
		log.Error().
			Str("OLD_FILE", r.Old).
			Str("NEW_FILE", r.New).
			Err(err).Msg(errReadData.Error())
		return nil, errReadData
	}

	if newData == nil || oldData == nil {
		return nil, errDataEmpty
	}

	resp := &model.CompareResponse{
		Created: make(map[string]*model.Data),
		Updated: make(map[string]*model.UpdatedData),
		Deleted: make(map[string]*model.Data),
	}

	wg.Add(1)
	go func() {
		defer wg.Done()
		for k, oldRec := range oldData {
			newRec, ok := newData[k]
			if !ok {
				resp.Deleted[k] = oldRec
			} else if *newRec != *oldRec {
				resp.Updated[k] = &model.UpdatedData{
					Old: oldRec,
					New: newRec,
				}
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for k, newRec := range newData {
			_, ok := oldData[k]
			if !ok {
				resp.Created[k] = newRec
			}
		}
	}()

	wg.Wait()

	return resp, nil
}

func (s comparer) read(ctx context.Context, filename string, wg *sync.WaitGroup, cData chan<- map[string]*model.Data, cErr chan<- error) {
	defer wg.Done()

	file, err := s.fileStore.Read(ctx, filename)
	if err != nil {
		cErr <- err
		return
	}

	data, curErr := s.dataStore.Read(ctx, file)
	if curErr != nil {
		cErr <- err
	}
	cData <- data
}
