package comparer

import (
	"context"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/metrics"
	model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"
	"time"
)

const methodCompare = "storage/comparer/Compare"

type metricMiddleware struct {
	Service
	monitor *metrics.Monitor
}

func NewMetrics(monitor *metrics.Monitor) ServiceMiddleware {
	return func(next Service) Service {
		return &metricMiddleware{
			Service: next,
			monitor: monitor,
		}
	}
}

func (m *metricMiddleware) Compare(ctx context.Context, r model.CompareRequest) (*model.CompareResponse, error) {
	defer m.monitor.Metrics.DurationInc(methodCompare, time.Now())
	resp, err := m.Service.Compare(ctx, r)
	m.monitor.Metrics.CountInc(methodCompare, err)
	return resp, err
}
