package comparer

import (
	"context"
	model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"
)

type Service interface {
	Compare(ctx context.Context, r model.CompareRequest) (*model.CompareResponse, error)
}
type ServiceMiddleware func(Service) Service
