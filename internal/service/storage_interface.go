package service

import (
	"context"
	model "gitlab.com/ololoproxdd-golang/comparer_2gis/internal/model/service"
	"os"
)

type FileStorage interface {
	Read(ctx context.Context, filename string) (*os.File, error)
}
type FileStorageMiddleware func(FileStorage) FileStorage

type DataStorage interface {
	Read(ctx context.Context, file *os.File) (map[string]*model.Data, error)
}
type DataStorageMiddleware func(DataStorage) DataStorage
