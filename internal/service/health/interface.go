package health

import "context"

type Service interface {
	Ping(ctx context.Context) string
}
