package health

import "context"

type service struct {
}

func New() Service {
	return &service{}
}

func (s service) Ping(context.Context) string {
	return "Pong"
}
