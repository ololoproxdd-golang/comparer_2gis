# comparer_2gis

Сервис который принимает на вход два прайс-листа (старый и новый) в формате csv.  
Сервис возвращает информацию о том:
* какие новые товары появились
* какие товары были обновлены
* какие товары пропали

## Сборка приложения
1. Установить на рабочую машину [golang версии 1.14](https://golang.org/dl/)
2. Установить переменные окружения, описанные в ```.env.dist```
3. Выполнить команду  
```go build -o comparer_2gis cmd/server/main.go ```
4. Появится бинарный файл с именем ```comparer_2gis```, который можно запустить 

## Тестовый пример

Выполнить POST запрос по эндопинту `/api/v1/compare`, указав в качестве тела запроса
```
{
       "old_file_path":"assets/old.csv",
       "new_file_path":"assets/new.csv"
}
```

Результатом данного запроса будет ответ
```
{
    "created": [
        {
            "name": "Mazeratti",
            "description": "Elite car",
            "price": "2000"
        }
    ],
    "deleted": [
        {
            "name": "Ferrari",
            "description": "Elite car",
            "price": "100000"
        },
        {
            "name": "Honda",
            "description": "Family car",
            "price": "100"
        }
    ],
    "updated": [
        {
            "name": "Audi",
            "updated_fields": [
                {
                    "field_name": "price",
                    "old_value": "1000",
                    "new_value": "2000"
                },
                {
                    "field_name": "description",
                    "old_value": "Premium car",
                    "new_value": "Elite car"
                }
            ]
        }
    ]
}
```

Сравниваемые файлы соответственно находятся в папке ```assets```

## API
Все эндпоинты, входные и выходные данные описаны в ```api/swagger.yml```

## Метрики
Сервис собирает метрики с помощью [prometheus](https://prometheus.io/), которые можно получить по эндпоинту ```/metrics```

## Тесты
Функциональные и юнит тесты запускаются в gitlab-ci.  
Для ручного запуска можно выполнить следующие команды
```
go test ./... -run TestUnit_[\S]* -v -coverprofile .testCoverage.txt
go test ./... -run TestFunctional_[\S]* -v -coverprofile .testCoverage.txt
```

## Зависимости
Управление зависимостями происходит через [gomod](https://blog.golang.org/using-go-modules)

## Логи
Все логи записываются в stdout и в файл, который находится по пути перменной окружения ```LOG_PATH```