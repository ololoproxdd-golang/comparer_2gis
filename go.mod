module gitlab.com/ololoproxdd-golang/comparer_2gis

go 1.14

require (
	github.com/go-kit/kit v0.10.0
	github.com/golang/mock v1.4.4
	github.com/gorilla/mux v1.7.4
	github.com/hashicorp/go-multierror v1.1.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/joho/godotenv v1.3.0
	github.com/nsf/jsondiff v0.0.0-20200515183724-f29ed568f4ce
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.7.1
	github.com/rs/zerolog v1.19.0
)
