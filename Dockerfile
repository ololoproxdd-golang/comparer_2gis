FROM golang:alpine AS builder
WORKDIR /go/src/cmd/server
COPY . .

ENV GO111MODULE on
ENV GOOS linux
ENV GOARCH amd64
ENV CGO_ENABLED 0

RUN apk add --no-cache git
RUN go get -d -v ./...
RUN go install -v ./...

FROM scratch

COPY --from=builder /etc/ssl /etc/ssl
COPY --from=builder /go/bin/server /go/bin/server

ARG APP_ENV
ENV APP_ENV=${APP_ENV}

ARG APP_HOST
ENV APP_HOST=${APP_HOST}

ARG APP_HTTP_PORT
ENV APP_HTTP_PORT=${APP_HTTP_PORT}


EXPOSE 8000
CMD [ "/go/bin/server" ]
