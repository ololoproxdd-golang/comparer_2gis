package main

import (
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/config"
	"gitlab.com/ololoproxdd-golang/comparer_2gis/internal/server"
)

func main() {
	srv := server.NewServer(config.LoadConfig())
	srv.Run()
}
